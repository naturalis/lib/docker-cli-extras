FROM docker:@version@

ARG TRIVY_IGNORE_URL="https://gitlab.com/api/v4/projects/41791412/repository/files/trivyignore/raw?ref=main"
ARG TRIVYIGNORE_ACCESS_TOKEN

RUN apk update \
    && apk upgrade \
    && apk add --no-cache bash \
    && apk add --no-cache zsh \
    && apk add --no-cache git \
    && apk add --no-cache jq \
    && apk add --no-cache libxml2-utils \
    && apk add --no-cache curl \
    && apk add --no-cache wget \
    && curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/main/contrib/install.sh | sh -s -- -b /usr/local/bin \
    && wget --header "PRIVATE-TOKEN: ${TRIVYIGNORE_ACCESS_TOKEN}" ${TRIVY_IGNORE_URL} "--output-document=/etc/.trivyignore"

COPY scan /usr/local/bin