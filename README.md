# Docker CLI Extras

This repository contains a Docker images wrapping various versions of the standard
[docker-cli](https://hub.docker.com/_/docker) image, the image customarily used to run
"Docker inside Docker".

The docker-cli-extras image serves two main purposes:

1. It allows you to write and run Bash or Z shell scripts. The standard docker-cli image
   only contains the Bourne shell (sh), which you may find too cumbersome and primitive.
2. It allows you to execute Trivy vulnerability scans within your Gitlab pipeline in a
   simple manner.

Besides that it contains various basic utilities that you may find useful inside a
docker-in-docker container.

## Contents of docker-cli-extras

- bash
- zsh (Z shell)
- git
- jq
- libxml2-utils
- wget
- curl
- trivy

## Usage

The docker-cli-extras image can be used wherever you would ordinarily use the docker-cli
image:

```yaml
Build Docker Image:
  image: registry.gitlab.com/naturalis/lib/docker-cli-extras:24.0.5-cli
  services:
    - docker:24.0.5-dind
  stage: build
  script:
    - docker/build-image.sh
```

(In this example build-image.sh could be a bash or zsh script.)

## Trivy Scans

Executing a Trivy scan in a Gitlab pipeline used to be a laborious process. It involved:

1. creating a temporary container from the official Trivy Docker image
2. teasing out the `trivy` executable from it
3. do a `wget` of our centrally maintained [.trivyignore](https://gitlab.com/api/v4/projects/41791412/repository/files/trivyignore/raw?ref=main) file
4. execute the `trivy` command with a number of arguments too large to sit comfortably
   inside `.gitlab-ci.yml` itself and thus usually offloaded into a separate shell script.

This can now be done with a single command.

Besides the utilities listed above, docker-cli-extras also already contains
the `.trivyignore` file, as well as a shell script named `scan`. This shell script
calls `trivy` with the arguments that have become customary within our pipelines. This
allows for a very simple "Trivy Scan" section in `.gitlab-ci.yml`:

```yaml
Scan Docker Image:
  stage: image-scan
  image: registry.gitlab.com/naturalis/lib/docker-cli-extras:24.0.5-cli
  script:
    - scan "${CI_REGISTRY_IMAGE}:temp"
```

## Builds and Versions

The [docker-cli-versions](docker-cli-versions) file contains all docker-cli versions which
we support &#8212; that is, for which we provide the corresponding docker-cli-extras
image. At the time of writing it looks like this:

```
23.0.6-cli
24.0.5-cli
24.0.6-cli
24.0.9-cli
cli
```

If your project's build pipeline for one reason or another needs a version that is not
present in the docker-cli-versions file, you can just add that version. When you then
commit/push your changes, a wrapper image for the version you added will be built and
become available via this repository's container registry.

Besides that, all versions of the docker-cli-extras image get built every night to ensure
they contain the latest version of `.trivyignore`. You can of course manually kick off a
build if you just updated `.trivyignore`.

The "latest" version of docker-cli also gets wrapped into a docker-cli-extras image every
night.

## docker-dind vs. docker-cli

Docker Hub provides two flavors of Docker images that themselves contain Docker
executables again:

- **docker-dind**, which contains the Docker daemon and does all the heavy lifting
- **docker-cli**, which contains the Docker REST client that can talk to a Docker daemon

In theory, when building Docker images inside a Gitlab pipeline, you might be able to
"configure out" how to do all your docker-in-docker work with just docker-dind. This image
_also_ contains the REST client (in fact it wraps the docker-cli image). However, this is
not Gitlab's preferred workflow. You should use the docker-cli **image** to run your shell
script in, and use Gitlab's docker-dind **service** as the endpoint for the `docker`
commands in your shell script:

```yaml
Build Docker Image:
  image: registry.gitlab.com/naturalis/lib/docker-cli-extras:24.0.5-cli
  services:
    - docker:24.0.5-dind
```

As the above example illustrates, you can and should still use the regular docker-dind
**service** next to the docker-cli-extras **image**.
