set -e

baseUrl='registry.gitlab.com/naturalis/lib/docker-cli-extras'

myDir=$(dirname "${0}")
baseDir="${myDir}/../"
baseDir=$(realpath "${baseDir}")

echo -n ${CI_REGISTRY_PASSWORD} | docker login -u ${CI_REGISTRY_USER} --password-stdin ${CI_REGISTRY}

while read -r version; do
  echo
  echo "@@@@@@@@@@@@@  Building docker-cli-extras:${version}  @@@@@@@@@@@@@"
  rm -f "${baseDir}/Dockerfile"
  cat "${baseDir}/template.Dockerfile" | sed s/@version@/${version}/ >> "${baseDir}/Dockerfile"
  image="${baseUrl}:${version}"
  docker build \
      --pull \
      --tag "${image}" \
      --file "${baseDir}/Dockerfile" \
      --build-arg "TRIVYIGNORE_ACCESS_TOKEN=${TRIVYIGNORE_ACCESS_TOKEN}" \
      "${baseDir}"
  docker push "${image}"
  if [ "${version}" = "cli" ]; then
    latest="${baseUrl}:latest"
    docker tag "${image}" "${latest}"
    docker push "${latest}"
  fi
done < "${baseDir}/docker-cli-versions"


rm -f "${baseDir}/Dockerfile"

